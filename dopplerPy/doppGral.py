#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  doppGral.py
#  
#  Copyright 2023 Sergio Daniel Roldán Bernhard <roldan.sergio@disroot.org>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
import sys 
import getopt
import os
import logging
import re 

from datetime import datetime
import pandas as pd
import numpy as np
import dopplerindclass as di
import  takeInfo as ti
#import cv2 as cv2
def stampLog(initTime):
    """
    Function stamplog 
    @param initTime timestamp
    @type timestamp
    """
    logging.info('From start: {0}'.format(datetime.now()-initTime))

def main(argv):
	inptDir=''
	oDir=''
	gammaExp=None#0.454545
	saveCfbx=False
	iniTime=datetime.now()
	#lineLog=0
	try:
		opts,args=getopt.getopt(argv,"hi:o:",["iDir=","oDir=", "gammaExp=","saveCfbx="])
	except getopt.GetoptError:
		print ('python3 doppGral.py  -i <inptDir> -o <oDir/> [--gammaExp <None>] [--saveCfbx <False>]')
		sys.exit(2)
	for opt,arg in opts:
		if opt =='-h':
			print ('python3 doppGral.py  -i <inptDir> -o <oDir/> [--gammaExp <None>] [--saveCfbx <False>]')
			sys.exit()
		elif opt in ("-i","--iDir"):
			inptDir=arg
		elif opt in ("-o","--oDir"):
			oDir=arg
		elif opt in ("-g", "--gammaExp"):
			gammaExp=float(arg)
		elif opt in ("-s","--saveCfbx"):
			if arg=='True':
				saveCfbx=True
	
	if not os.path.isdir(oDir):
		os.mkdir(oDir)
	
	##logging.info('Starting ... {1}'.format(lineLog, iniTime))
	logging.basicConfig(filename=os.path.join(oDir,'doppGral.log'), 
		level=logging.DEBUG, 
		filemode='w', 
		format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s', 
		datefmt='%d/%m/%Y %H:%M:%S')
	logging.info('Creating output dir:{0}'.format(oDir))
	logging.info('Input Directory:{0}'.format(inptDir))
	logging.info ('Output Directory {0}'.format(oDir))
	if saveCfbx:
		pth2cfbx=os.path.join(oDir,'cfbx/')
		if not os.path.exists(pth2cfbx):
			os.mkdir(pth2cfbx)
			logging.debug('Making cfbx directory:{}'.format(pth2cfbx))
	##rootName='usimage'
	reRun={'idPac':[],'year':[],'month':[],'day':[],'hour':[],'min':[],'seg':[],'idEx': [],'idOb':[]}
	##tStmps=tStmpsD=datetime.now()
	reDw={'idA':[],'idOb':[],'idPac':[], 'typ':[],'Grp':[],'idEx':[],'numY':[],'stamp':[],'arch':[],'path':[],  'vfp':[],'vfEp':[], 'vfn':[], 'vfEn':[], 'vft':[],\
		'vfEt':[],'vap':[],'vaEp':[],'van':[],'vaEn':[], 'vat':[],'vaEt':[]}
	reDopDf=pd.DataFrame.from_dict(dict(keys=['idA','idOb','idPac','typ','Grp','idEx',  'stamp','arch','path' ,  'vfp','vfEp',  'vfn',  'vfEn', 'vft','vfEt','vap','vaEp','van',  'vaEn',  'vat','vaEt']) ,orient='index')
	if (os.path.exists(inptDir) and os.path.isdir(inptDir) and os.path.isdir(oDir)):
		logging.debug('Processing...{0}'.format(inptDir))#'cadena':[],
		cntOb=cntAvi=cntJpg=0#cntArch=0
		for (root,dirc,arch) in os.walk(inptDir,topdown=False):
			cntOb+=1
			##stampLog(iniTime)
			#breakpoint()
			for name in arch:
				if (name[-3:]=='avi'):
					cntAvi+=1
					cadR=os.path.join(root,name)
					try:
						res=ti.takeInfo(cadR)
						logging.info('Information taked by AVI:{}'.format(cadR))
					except:
						if (isinstance(res,dict)):
							logging.info("# "+str(cntOb)+' Unknow error(Error4) {0} '.format(name))
						elif(isinstance(res,int)):
							logging.error('May be loss a pacient Id: {0}'.format(name))
					else:
						for i in reRun.keys():
							if i=='idOb':
								reRun[i].append(str(cntOb))
							else:
								reRun[i].append(res[i])
					finally:
						logging.info('{0!s}--&&--{1}\n{2}\n'.format(cntOb,name,np.array([*reRun.values()])[:,-1]))
				elif (name[-3:]=='jpg'):
					cntJpg+=1
					try: # Processing jpg bmode doppler image
						cadJ=os.path.join(root,name)
						resJ=ti.takeInfo(cadJ)
						logging.info('Processing {} idPac:{}'.format(name, resJ['idPac']))
						dop=di.doppimg(cadJ, gamma=gammaExp)
						if saveCfbx:
							try:
								#di.doppimg(cadJ, gamma=1).colorBx.save(os.path.join(pth2cfbx, name[:-4]+'.png'))
								logging.info('Guardaría el png')
								#cv2.imwrite(os.path.join(pth2cfbx, name[:-4]+'.png'),cv2.cvtColor(di.doppimg(cadJ, gamma=1).colorBx,cv2.COLOR_BGR2RGB))
							except:
								logging.info('Error:'+os.path.join(pth2cfbx, name))
						logging.info('Ending processing {}'.format(name))
					except:
						logging.warning('Not doppler mode image {0}'.format(name))
					else:
						if (dop.cal!=-1):
							resJ=ti. takeInfo(cadJ)
							Grp=cadJ.split('.')[-2]
							regMatch=re.search(r'([C|P])([0-9]+)$',Grp[-10:])
							try:
								typeVasc=regMatch.group(1)
								vascGrp=regMatch.group(2)
							except AttributeError:
								typeVasc='nd'
								vascGrp='99'
								logging.warning(str(Grp)+'No match Vascularization type and/or vascularization Group. Remember *_[C|P][0-9]+ format for classification. : {0}'.format(name))
							else:
								logging.info('Successs!!: {0}'.format(name))
							finally:
								info=dop.vasc(type='values')
								dirF=os.path.split(os.path.relpath(cadJ, start = inptDir))
								stampi=pd.to_datetime(resJ["year"]+"-"+resJ["month"]+"-"+resJ["day"]+" "+resJ["hour"]+":"+resJ["min"]+":"+resJ["seg"])
								reD={'idA':cntJpg,'idOb':str(cntOb),'idPac':resJ['idPac'], 'typ':typeVasc,\
									'Grp':vascGrp,'idEx':resJ['idEx'],'numY':resJ['numY'],'arch':dirF[1], 'stamp':stampi, 'path':dirF[0]}
								reD.update(info)
								for u in reDw.keys():
									reDw[u].append(reD[u])
								logging.info(pd.DataFrame.from_dict([reD],orient='columns' ))	
						del(dop)
						print(' '+str(cntOb)+'/'+str(cntJpg), end='')
				stampLog(iniTime)
	elif(not os.path.isdir(inptDir)):
		if (os.path.isdir(oDir)):
			logging.error('{0} is not a DIRECTORY '.format(inptDir))
			sys.exit(2)
	else:
		logging.error('review path: {0}'.format(inptDir))
		sys.exit(2)
	#breakpoint()
	logging.info('----Image reading finished----\n'+str(stampLog(iniTime)))
	reDopDf=pd.DataFrame.from_dict(reDw,orient='columns' )
	
	dopsFull= reDopDf.sort_values(by=['idEx', 'idPac', 'stamp', 'typ', 'Grp'])#pd.merge(dfg, reDopDf, on =['idOb','idPac'], how ="inner")
	dopsFull.to_excel(os.path.join(oDir+"dopsFull.xlsx"))
	if 	saveCfbx:
		logging.info('Salvando plots')
		import plotGrid as pg
		import matplotlib as  mpl
		#mpl.use("pgf"
		mpl.rcParams.update({"pgf.texsystem": "xelatex",'font.size':10,'font.family': 'serif','text.usetex': True,'pgf.rcfonts': True})
		labPac=np.unique(dopsFull['idPac'], return_inverse=False)
		logging.debug(labPac)
		for l in labPac:
			idX=dopsFull.loc[dopsFull['idPac']==l]#labPac[idxPx]]
			#pX=pg.plotCfbx(dopsFull,idxs=idX.index ,labels=[],  pth0=inptDir)
			pX=pg.plotCfbx(idX,idxs=None ,labels=None,  pth0=inptDir)
			pX.savefig(os.path.join(pth2cfbx,str(l)+'.pdf'), backend='pdf')
			mpl.pyplot.close()
	cadEnd='\nFinishing Process {0} after {1} secs\n \n{2} Observations\n {3} Videos\n{4} jpg Images'.format(datetime.now(),datetime.now()-iniTime,cntOb, cntAvi, cntJpg)
	logging.info(cadEnd)
	print(cadEnd)

if __name__=="__main__":
	main(sys.argv[1:])
#	p=re.compile(r"""Gamma\:""",re.VERBOSE)
