#-*-coding: utf8 -*-
# Generar documentación con 
# epydoc -n dopplerPy --html  -v -o ~/Programas/dopplerPy/html/  dopplerPy/
# Sincronizar:
#  Ejecutar como

#  sudo rsync  -t -v -e ssh ..../html/*.* 172.17.228.78:/home/roldansergio/publico/.../
__docformat__="restructuredtext es"
__author__='Sergio Roldán'
__copyright__=''
__license__=''
__date__='27/05/2022'
import cv2 as cv2
import logging 
import os
import numpy as np
import pandas as pd
import statsmodels.api as sm
import subprocess

def gamma_correct(image:np.array, gamma:float=1):
	"""
	:Parameters:
	-`image`: numpy array
	-`gamma`: float: gamma encoded
	"""
	#as https://pyimagesearch.com/2015/10/05/opencv-gamma-correction/
	# build a lookup table mapping the pixel values [0, 255] to
	# their adjusted gamma values
	invGamma = 1.0 / gamma
	table = np.array([((i / 255.0) ** invGamma) * 255
		for i in np.arange(0, 256)]).astype("uint8")
	# apply gamma correction using the lookup table
	logging.info('Gamma correction:{}'.format(invGamma))
	return cv2.LUT(image, table)

class doppimg(object):
	"""
	
	A class to process Ultrasound Colour Doppler frames exported 
	from cine-loops, to quantize blood from into a single frame. 
	The Doppler mode, intended to add color-coded quantitative information concerning the
	relative velocity and direction of fluid motion within the B-mode image
	Doppler image. 
	Ultrasound equipment Chison, ECO6; Wuxi, Jiangsu, China
	in color flow mode [CF] ([CF]: FRQ 10.0 M, PRF 3000 Hz, WF 3, GN 157).
	Chison Eco 6.
	"""
	def __init__(self,pth:str,colorSK:float=14.4,gamma:float=None,pxBy5cm:int=390, deltaNuInst:float=0.1):
		"""
		**doppimg** Constructor
		@param pth path to image (jpg)
		@type str
		@param colorSK Absolute value of maximum scale (defaults to 14.4)(cm/s)
		@type float (optional)
		@param gamma encoded. 1: not decoding,-1: automatic reading gamma from image metadata  (defaults to None)
		@type float (optional)
		@param pxBy5cm geometric scale in pixels (defaults to 390)
		@type int (optional)
		@param deltaNuInst instrumental error (defaults to 0.1)(cm/s)
		@type float (optional)
		@return:  doppimg intance
		"""
		logging.info("Setting DopplerIndClass instance. Reading ..{0}\n{1}\n-----".format(os.path.basename(pth), pth))
		self.pth=pth
		if gamma==None:
			try:
				import re
				readedMeta=subprocess.run(["identify","-verbose",pth],capture_output=True)
				readedGamma=float(re.split(r': ',re.findall(r'\bGamma\: [0-9]+.[0-9]+',str(readedMeta.stdout))[0],1)[1])
				logging.info('Gamma readed from image:{0}'.format(readedGamma))
				print('Gamma readed from image:{0}'.format(readedGamma))
				self.gamma=readedGamma
			except:
				logging.debug('Exception from reading gamma')
				print('Exception from reading gamma')
				self.gamma=1.
			finally:
				logging.info('Gamma asignation finishing.{0}'.format(self.gamma))
		else:
			self.gamma=gamma
# float(re.split(r': ',re.findall(r'\bGamma\: [0-9]+.[0-9]+',str(res.stdout))[0],1)[1])
		self.pxBy5cm=pxBy5cm
		self.colorSK=colorSK#ver de unificar
		self.deltaA=50/(self.pxBy5cm**3)
		self.deltaNuInst=deltaNuInst
		try:
			#self.im=Image.open(self.pth)
			self.im=cv2.cvtColor(cv2.imread(self.pth), cv2.COLOR_BGR2RGB)
			#self.im=cv2.imread(self.pth)
			
		except OSError:
			logging.warning("image file not identified")
			self.cal='No Doppler B Mode image'
		else:
			self.im=gamma_correct(self.im, self.gamma)
			self.cal=self.colCal()
			xo,yo,xf,yf=self.getColorBox(mrg=3)
			self.colorBx=self.im[yo:yf, xo:xf]
		
	@property
	def Kx(self):
		"""
		Relative horizontal scale
		"""
		return(self.im.shape[1]/776)
	@property
	def Ky(self):
		"""
		Relative vertical scale 
		"""
		return(self.im.shape[0]/568)
	@property
	def pixelA(self):
		"""
		Areal representation by pixel (cm^2/px)
		"""
		sKly=5. #(cm)
		return(sKly**2/(self.pxBy5cm*self.Ky)**2)
	
	def sampleDopplerColorSkl(self,absolute=False, kCs=[0, 83,  90, 170]):
		"""
		Public method Extract and sample colour scale
		@param absolute scale of results. True: Hue in degrees, Sat and Value as percents. False: unsigned 8 bit integer(defaults to False)
		@type boolean (optional)
		@param kCs [top row positive range,bottom row positive range,top row negative range and bottom row negative range(defaults to [0, 83,  90, 170])
		@type list (optional)
		@rtype pandas.DataFrame
		"""
		areaP=np.array([646*self.Kx,70*self.Ky,656*self.Kx,(70+kCs[1])*self.Ky], 'int')
		areaN=np.array([646*self.Kx,(70+kCs[2])*self.Ky,656*self.Kx,(70+kCs[3])*self.Ky], 'int')
		g1hsvP=cv2.cvtColor(self.im[areaP[1]:areaP[3], areaP[0]:areaP[2]],cv2.COLOR_RGB2HSV)
		g1hsvN=cv2.cvtColor(self.im[areaN[1]:areaN[3], areaN[0]:areaN[2]],cv2.COLOR_RGB2HSV)
		colMinP,colMinN=[g1hsvP.min(axis=0).min(axis=0),g1hsvN.min(axis=0).min(axis=0)]
		#colMaxP=g1hsvP.max(axis=0).max(axis=0)
		colMaxN=g1hsvN.max(axis=0).max(axis=0)
		colRanges=np.row_stack((colMinP,[[50, 255, 255]],colMinN,colMaxN))
		if absolute:
			colRanges[:,0]=np.array(colRanges[:,0],'double')/180*360
			colRanges[:,1]=np.array(colRanges[:,1],'double')*100/255
			colRanges[:,2]=np.array(colRanges[:,2],'double')*100/255
			return(pd.DataFrame(np.round(np.array(colRanges,'float'),1),columns=['Hue$[^{\\circ}]$','Saturation$[\\%]$','Value$[\\%]$'],index=['+m','+x','-m','-x']))
		else:      
			return(pd.DataFrame(np.array(colRanges,'int'),columns=['Hue','Saturation','Value'], index=['pm', 'px', 'nm', 'nx']))
	def colCal(self, kC=[1, 83, 90, 168]):
		"""
		Calibration of scale colors to flow velocity
		:param kCs rows of extrem values
		:type integers list
		:return
		:rtype dictionary of linear models and reference table
		"""
		kCs=np.array([kC[0]*self.Kx, kC[1]*self.Ky,kC[2]*self.Kx ,kC[3]*self.Ky ], 'float').astype(int)
		colorBar=np.array((646*self.Kx,71*self.Ky,656*self.Kx,239*self.Ky), 'float').astype(int)
		calColIm=cv2.cvtColor(self.im[colorBar[1]:colorBar[3], colorBar[0]:colorBar[2]], cv2.COLOR_BGR2RGB)
		calCol=calColIm.mean(axis=1)
		mColP=calCol[kCs[0]:kCs[1], :].mean(axis=0).round(0)
		mColN=calCol[kCs[2]:kCs[3], :].mean(axis=0).round(0)
		if ( mColP[2]>(mColP[0]+10) and mColN[0]>(mColN[2]+10)): # testing if cropped region have colors
			freqSamp=4
			kP=list(range(kCs[1],kCs[0]-1,-freqSamp*int(round((kCs[1]-kCs[0])/(kCs[1]-kCs[0]),0))))
			kN=list(range(kCs[2],kCs[3]-1,freqSamp*int(round((kCs[3]-kCs[2])/(kCs[3]-kCs[2]),0))))
			yP=(np.asarray(list(range(len(kP)))))*(self.colorSK*freqSamp/((kCs[1]-kCs[0])))
			yN=(np.asarray(list(range(len(kN)))))*(self.colorSK*freqSamp/((kCs[3]-kCs[2])))
			xPo=calCol.mean(axis=1)[kP]# Positive sector; means trought columns
			xNo=calCol.mean(axis=1)[kN]
			xP=sm.add_constant(xPo)
			xN=sm.add_constant(xNo)
			lmP=sm.OLS(yP,xP).fit()
			lmN=sm.OLS(yN,xN).fit()
			logging.debug('Positive flows calibration summary:\n'+str(lmP.summary()))
			logging.debug('Negative flows calibration summary:\n'+str(lmN.summary()))
			dfNi=pd.DataFrame({'dir':'n', 'row':kN, 'L':xNo, 'nu':yN}, index=kN)
			dfPi=pd.DataFrame({'dir':'p', 'row':kP, 'L':xPo, 'nu':yP}, index=kP)
			dCal=pd.concat([dfPi, dfNi])
			resu={'lmP':lmP,'lmN':lmN,'dCal':dCal, 'minP':-1*lmP.params[0]/lmP.params[1],\
				'minN':-1*lmN.params[0]/lmN.params[1], 'colRanges':self.sampleDopplerColorSkl(absolute=False)} #xNo[pN.__ge__(0)][0]}
			return(resu)
		else:
			logging.warning('No Doppler image {0}'.format(os.path.basename(self.pth)))
			return(-1)
#	@property
	def getColorBox(self,mrg=3,colR=np.array([[27, 89,204],[ 41,206, 252]])):
		"""
		Find bounding box of yellow line and taking off 'mrg' pixels
		@param mrg buffer pixels (defaults to 3)
		@type integer (optional)
		@param colR color definition HSV (defaults to np.array([[27, 89,204],[ 41,206, 252]]))
		@type numpy integer array (optional)
		@return [x upper left, y upper left,x bottom right,y bottom right]
		@rtype integer list
		"""
		imDopBox,orig=self.displayBx()['im'],self.displayBx()['origin']
		img2=cv2.cvtColor(imDopBox, cv2.COLOR_RGB2HSV)
		colR=np.uint8(colR)
		x00,y00,w0,h0=cv2.boundingRect(cv2.inRange(img2,colR[0,:],colR[1,:]))
		x0, y0=orig
		boxDoppler=np.array([x0+x00+mrg,y0+y00+mrg,x0+x00+w0-mrg,y0+y00+h0-mrg], 'int')
		logging.debug("Color box relative pos: xul:{0} yul:{1} xlr: {2} width{3}, height: {4} pixel of buffer".format(x00,y00,w0,h0, mrg))
		logging.info("Color box asolute pos: xul:{0} yul:{1} xlr: {2} ylr{3}".format(boxDoppler[0],boxDoppler[1],boxDoppler[2],boxDoppler[3]))
		return ((boxDoppler))
		
	def displayBx(self, ref=[138, 87, 504, 540]):
		"""
		Crop Display Box from display image
		@param ref list of xUL,yUL,xBR,yBR (defaults to [138, 87, 504, 540])
		@type list (optional)
		@return ['im'] PIL image ['origin']: [x,y]UR corner
		@rtype dictionary 
		"""
		x0, y0, x1, y1=np.uint(np.array(ref)*[self.Kx, self.Ky, self.Kx, self.Ky])
		dippBox=self.im[y0:y1,x0:x1]
		logging.info("\n Display Box (Image coord): \n Left col0:{0}\t Top row:{1} \t Right col:{2} \t Bottom row:{3}\n".format(x0,y0,x1,y1))
		logging.info("\n Width(cols): {0}\t Height(rows):{1} ".format(dippBox.shape[1], dippBox.shape[0]))
		return({'im':dippBox,'origin':[x0,y0]})

	def vasc(self,tst1= None, type='values', deltaInst=0.1):
		"""
		Quantitative vascularization (as values) dictionary or Doppler color box mask of positive and negative flow 
		:param type: 'values' or 'mask'
		:return dictionary of values or masks
		""" 
		if tst1 is None :
			tst1=self.colorBx
		if type=='mask':
			HSV=cv2.cvtColor(tst1, cv2.COLOR_RGB2HSV)#.convert('HSV'))# HSV have row,col coord
			intV=np.array(tst1).mean(axis=2)
			maskN=cv2.bitwise_and(cv2.inRange(HSV, np.array(self.cal['colRanges'].loc['nm']), np.array(self.cal['colRanges'].loc['nx'])),\
			cv2.inRange(intV, self.cal['minN'],256))
			maskPc=cv2.bitwise_and(cv2.inRange(HSV, np.array(self.cal['colRanges'].loc['pm']), np.array(self.cal['colRanges'].loc['px'])),\
			cv2.inRange(intV, self.cal['minP'],256))
			maskP=cv2.bitwise_and(maskPc,cv2.inRange(intV, self.cal['minP'],255))
			mskG=cv2.merge(( np.uint8(intV* maskP/255),np.zeros (shape=maskP.shape, dtype='uint8'), np.uint8(intV* maskN/255 )))
			mask={'pos':maskP, 'neg':maskN, 'maskG':mskG}
			return(mask)
		elif type=='values':
			def deltanu (mod,x, alpha=0.05):
				from scipy import stats
				den=(mod.model.data.exog[:,1]).var()#*mod.nobs # Denominator of Formula error
				xmed=-mod.model.data.exog[:,1].mean()
				pars=1+1/mod.nobs+(x-xmed)**2/(den)
				tc=stats.t(df=mod.nobs-2).ppf((1-alpha/2))
				deltaNuReg=tc*(mod.mse_resid*pars)**(.5)
				return((deltaNuReg**2+deltaInst**2)**.5)
			intV=np.array(tst1).mean(axis=2)
			maskN=self.vasc(type='mask')['neg']
			maskP=self.vasc(type='mask')['pos']
			vapi=self.cal['lmP'].predict(sm.add_constant(intV.reshape(intV.size,1))).reshape(intV.shape)*(maskP/255)
			deltaVfp=round(((deltanu(self.cal['lmP'],intV)**2*(maskP/255)*self.pixelA**2+vapi**2*self.deltaA**2).sum())**.5, 4)
			vani=self.cal['lmN'].predict(sm.add_constant(intV.reshape(intV.size,1))).reshape(intV.shape)*(maskN/255)
			deltaVfn=round(((deltanu(self.cal['lmN'],intV)**2*(maskN/255)*self.pixelA**2+vani**2*self.deltaA**2).sum())**.5, 4)#Error negatives
			fp, fn=vapi.sum(), vani.sum() # totalizing flows [cm/s] arround image
			vfp, vfn=round(fp*self.pixelA, 4), round(fn*self.pixelA, 4) # scaling flows with pixel Area
			vap, van=round((maskP/255).sum()*self.pixelA, 4), round((maskN/255).sum()*self.pixelA, 4)
			vaEp, vaEn=round(((maskP/255).sum()**2*self.deltaA**2+self.pixelA)**.5, 4), round(((maskN/255).sum()**2*self.deltaA**2+self.pixelA)**.5, 4)
			out={'vfp':vfp,'vfEp':deltaVfp,'vfn':vfn,'vfEn':deltaVfn,'vft':round(vfn+vfp, 3), 'vfEt':round((deltaVfp**2+deltaVfn**2)**.5, 3),\
				'vap':vap,'vaEp':vaEp,'van':van, 'vaEn':vaEn, 'vat':round(van+vap, 3), 'vaEt':round((vaEp**2+vaEn**2)**.5, 3)}
			#logging.info("\n vfp \t vfn \t vft \t vap \t van \t vat\n {vfp}\t{vfn} \t{vft} \t {vap} \t {van} \t {vat}".format(**out))
			logging.info(pd.DataFrame.from_dict(out,orient='index'))
			return(out)
		else:
			logging.warning(type+' must be "values" or "mask"')
