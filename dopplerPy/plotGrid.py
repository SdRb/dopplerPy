#  Copyright 2023 Sergio Daniel Roldán Bernhard <roldan.sergio@disroot.org>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import os
import statsmodels.api as sm
import dopplerindclass as di

def plotCfbx(dbRdb,idxs=None, pth0=None, labels=None, vars=['img','vfp','vfn','vft']):
	typs=["C","P"]
	dbRdb['date']=dbRdb['stamp'].dt.strftime('%y-%m-%d')+'|'+dbRdb['numY']
	dbRdb['lab']= dbRdb['idPac']+dbRdb['typ']+dbRdb['Grp'].astype(str).str.zfill(0)
	indLabs=dbRdb['lab'].copy()
	if idxs is None:
		idxs=list(range(len(dbRdb)))
	if labels is not None and len(labels)==len(idxs):
		indLabs.loc[idxs]=labels
	else:	
		print("Labels must be same length than idxs")
	titles=['','$\\Phi_{(+)}$','$\\Phi_{(-)}$','$\\Phi$']#
	lab, ind= np.unique(dbRdb['typ'], return_inverse=True)
	#ind=np.logical_not(Ind).astype(int)
	figV, ax = plt.subplots(len(idxs),4,figsize=(10,len(idxs)*2.1))#,sharex=True)
	figV.subplots_adjust(hspace=0.05,wspace=0.25)
	for j in range(len(idxs)):
		ax[j,0].imshow(di.doppimg(os.path.join(pth0,dbRdb['path'].iloc[idxs[j]],dbRdb['arch'].iloc[idxs[j]]), gamma=1).colorBx)
		ax[j,0].set(yticks=[],xticks=[])#,x_margin=.1)
		ax[j,0].set(ylabel=indLabs.iloc[idxs[j]])
		for i in range(len(vars)):
			if i !=0:            
				sm.graphics.violinplot([dbRdb[vars[i]].loc[dbRdb['typ'] == id] for id in typs], ax=ax[j,i], labels=typs,  \
					plot_opts={'bean_lw':0.05, 'cutoff_val':1.8, 'cutoff_type':'std','violin_fc':'lightgray','violin_width':1,\
								'label_fontsize':'small','violin_alpha':0.5,'violin_lw':0.15, \
								'label_rotation':0})
				ax[j,i].scatter(x=ind[idxs[j]]+1, y= dbRdb[vars[i]].iloc[idxs[j]],s=20, marker="x", color="red", alpha=1,zorder=10)
				ax[j,i].annotate(round( dbRdb[vars[i]].iloc[idxs[j]],2), xy=(ind[idxs[j]]+1, dbRdb[vars[i]].iloc[idxs[j]]), xycoords='data',\
					xytext=(0.5, .95), textcoords='axes fraction',\
					va='top', ha='center',\
					bbox=dict(boxstyle="round", fc="lightgrey", ec="darkgrey",alpha=0.5),\
					arrowprops=dict(arrowstyle="-|>", color='lightgray',\
									connectionstyle="angle,angleA=-90,angleB=180,rad=5",relpos=(0.5, 0.),fc="k"))
				ax[j,i].set(yticks=[0,1,2,3,4,5,6])
				if i>1:    
					ax[j,i].set(ylabel='')
				else:
					ax[j,i].set(ylabel='$(cm^3/s)$')
				if j==0:
					ax[j,i].set(title=titles[i])
				if j!=len(idxs)-1:
					ax[j,i].set(xticklabels=[])
				else:
					ax[j,i].set(xticklabels=["cBf","pBf"])
				if (i ==(len(vars)-1)):# and labels is not None:
					ax2=ax[j,i].twinx()
					ax2.set_ylabel(dbRdb['date'].iloc[idxs[j]], rotation=-90,horizontalalignment="center", \
					verticalalignment="center",rotation_mode="anchor", transform_rotates_text=True)
					ax2.set(yticklabels=[], yticks=[])
	figV.set_size_inches(w=9, h=len(idxs)*1.7)
	return(figV)
