#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  takeInfo.py
#  
#  Copyright 2023 Sergio Daniel Roldán Bernhard <roldan.sergio@inta.gob.ar>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
__docformat__="restructuredtext es"
__author__='Sergio Roldan'
__copyright__=''
__license__=''
__date__='27/05/2022'

#import sys
#import getopt
#import os
#import re as re
#from datetime import datetime
#import pandas as pd
#import numpy as np

def takeInfo(cadR,prefix='usimage'):
	"""
	Take information of time stamp, pacient an examination identifier from path file string of a single image.
	Following exported file directory structure from Ultrasound Doppler device (CHIson) 
	:param cadR
	:type cadR str
	:param prefix of images or videos
	"""
	fchPac=cadR.split("/")[-3]
	try:
		pac=fchPac[fchPac.rindex("_")+1:]
	except:
		return(fchPac)
	idx=fchPac.split("-")[-1]
	idEx=idx[:idx.rindex("_")]
	[ano,mes,dia]=fchPac.split("-")[0:3]
	cadSin=prefix+ano+mes+dia
	indexTime=cadR.rindex(cadSin)+len(cadSin)+len(idEx)
	hms=cadR[indexTime:indexTime+6]
	numY=cadR[indexTime+6:indexTime+9]
	hora=hms[0:2]
	minu=hms[2:4]
	segs=hms[4:]
	listResult={'year':ano,'month':mes,'day':dia,'hour':hora,'min':minu,'seg':segs,'idEx': idEx,'numY':numY,'idPac':pac}
	return(listResult)

if __name__ == '__main__':
	print("Exitoso")

